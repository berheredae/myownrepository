import express from 'express';
import ReportController from '../../controllers/reporting/reportController';
import auth from '../../middlewares/auths';

const router = express.Router();



const reportingRoutes = (dependencies)=>{
    const reportController = new ReportController(dependencies);
    router.post("/customers-count",auth("Read Report","Read reports"), async(req,res,next)=> await reportController.getTotalCustomers(req,res,next));

    router.post("/orders",auth("Read Report","Read Reports"),async (req,res,next)=> await reportController.getOrders(req,res,next));

    router.post("/branchs",auth("Read Report","Read Reports"),async (req,res,next)=> await reportController.getBranchs(req,res,next));

    router.post("/stores",auth("Read Report","Read Reports"),async (req,res,next)=> await reportController.getStores(req,res,next));

    return router;
}



export default reportingRoutes;
