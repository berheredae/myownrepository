/**
 * load all required routes...
 */
// import projectDependencies from './config/projectDependencies';
import projectDependencies from '../config/projectDependencies';
import staffAccount from "./staffAccount/staffAccountRoute";
    import customerRoutes from "./customer/customerRoute";

    // Admin
    import branchRoutes from "./admin/branchRoute";

    //sales
    import productRoutes from "./sales/productRoute";
    import commericalOrderRoutes from "./sales/commericalOrderRoute";

    //System
    import accountManagementRoute from "./system/accountManagementRoute";
    import authorizationRoute from "./system/authorizationRoute";

    //Store
    import storeRoute from "./inventory/storeRoute";

    //Distribution
    import freightPriceRoute from "./distribution/freightPriceRoute";
    import vehicleRoute from "./distribution/vehicleRoute";
    import vehicleCompaniesRoute from "./distribution/vehicleCompaniesRoute";
    import carriageRoute from "./distribution/carriageRoute";
    import reportingRoutes from "./reporting/reportingRoute";


 function load_routes(router){
    
    router.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE");
        return res.status(200).json({});
    }
    next();
    });
    //router.use('/products', productRoutes);
    router.use("/api/customer", customerRoutes);
    router.use("/api/staff", staffAccount);
    router.use("/api/branch", branchRoutes);

    //Sales
    router.use("/api/product", productRoutes);
    router.use("/api/commercialorder", commericalOrderRoutes);
    // System
    router.use("/api/accountmanagement", accountManagementRoute);
    router.use("/api/authorization", authorizationRoute);

    //Inventory
    router.use("/api/store", storeRoute);

    //distribution
    router.use("/api/freightprice", freightPriceRoute);
    router.use("/api/vehicle", vehicleRoute);
    router.use("/api/vehiclecompanies", vehicleCompaniesRoute);
    router.use("/api/carriage", carriageRoute);


    // reporting
    router.use("/api/reports",reportingRoutes(projectDependencies));

    router.use((req, res, tekeze) => {
    const error = new Error("Not Found");
    error.status = 404;
    tekeze(error);
    });
    router.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
        message: error.message,
        status: error.status,
        },
    });
    });
    return router;
 }

 export default load_routes;