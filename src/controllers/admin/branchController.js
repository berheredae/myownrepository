import Controller from "../controller";
import Service from "../../Services/admin/branchService";
import Model from "../../models/admin/branchModel";

// const model = new Model().getModel();
const service = new Service(Model);

class branchController extends Controller {
  constructor(service) {
    super(service);
  }
  async getVerified(req, res) {
    const result = await service.getByField(
      {
        status: "Verified",
      },
      { log: 0, store: 0 }
    );
    res.status(result.statusCode).send(result);
  }
  async getLogs(req, res) {
    const result = await service.getById(req.params.id, { log: 1 });
    res.status(result.statusCode).send(result);
  }


  async addStores(req,res){
      try {
        const {branch_id,store_ids} = req.body;
        const branch = await service.addStores(branch_id,store_ids);
        if(!branch) return res.status(400).json({result:{errors:["Make sure your provided valid branch and store information"]}});
        return res.status(200).json({result:{branch}});
      } catch (error) {
        console.log("Error on addstore :> ",error);
      }
  }

  async deleteStore(req,res){
    try {
      const {branch_id,store_id} = req.params;
      const branch = await service.deleteStore(branch_id,store_id);
      return res.status(200).json({result:{branch}});
    } catch (error) {
      console.log("Error on deleteStore:> ",error);
      res.status(400).json(error);
    }
  }

}

export default new branchController(service);
