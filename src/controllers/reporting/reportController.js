import BaseController from "../baseController";


class ReportController extends BaseController {
    constructor(dependencies){
        console.log("the dependiencies : ",dependencies);
        super(dependencies.reportService);
    }


    async getTotalCustomers(req,res){
        try {
            const {customerClass,status} = req.body.filter || {};
            const result = await this.service.getTotalCustomers({customerClass,status});
            return res.json({result:result});
        } catch (error) {
            console.log("Error occured in ReportController : ",error);
            return res.status(500).json({error:error});
        }
    }

    async getOrders(req,res){
        try {
            console.log("getting reports on orders");
            // those are just the minimum order filtering mechanisms.
            const {status,orderType,productType} = req.body;
            const result = await this.service.getOrders({status,orderType,productType});
            return res.json({result});
        } catch (error) {
            console.log("Error while getting orders: ",error);
            return res.status(500).json({error});
        }
    }

    // will implement get stores and get branchs once we know how they are related and organized/structured.

    async getStores(req,res){
        try {
            console.log("Getting reports on stores");
            const {branch_id} = req.body;
            const result = await this.service.getStores(branch_id,{});
            return res.json({result});
        } catch (error) {
            console.log("Error occured : ",error);
            return res.status(500).json({error});
        }
    }

    async getBranchs(req,res){
        try {
            console.log("Getting reports on branchs");
            const result = await this.service.getBranchs({});
            console.log("result : ",result);
            return res.json({result});
        } catch (error) {
            console.log("Error ocured : ",error);
            return res.status(500).json({error})
        }
    }
}



export default ReportController;
