// robel- new

class BaseController {
    constructor(service){
        this.service = service;
        console.log("The base controller is working based on the service : ",service);
    }

    async create(req,res){
       try {
        const result = await this.service.create(req.body);
        return res.status(200).json({result:result});
       } catch (error) {
           return res.status(500).json({error:error});
       }
    }

    async removeById(req,res){
        try {
            const {id} = req.body || req.params;
            const result = await this.service.removeById(id);
            return res.status(200).json({result:result});
        } catch (error) {
           return res.status(500).json({error:error}); 
        }
    }

    async remove(req,res){
        try {
            const {filter} = req.body;
            const result = await this.service.remove(filter);
            return res.status(200).json({result:result});
        } catch (error) {
           return res.status(500).json({error:error}); 
        }
    }

    async updateById(req,res){
      
        try {
            const {id} = req.params;
            const {data} = req.body;
            const result = await this.service.updateById(id,data);
            return res.status(200).json({result:result});
        } catch (error) {
            return res.status(500).json({error:error});
        }
    }

    async update(req,res){
      try {
        const {filter} = req.body;
        const {data} = req.body;
        const result = await this.service.update(filter,data); 
        return res.status(200).json({result:result});
      } catch (error) {
          return res.status(500).json({error:error});
      }
    }

    async getById(req,res){
        try {
            const {id} = req.params || req.body;
            const result = await this.service.getById(id);
            return res.status(200).json({result:result});
        } catch (error) {
            return res.status(500).json({error:error});
        }
    }

    async get(req,res){
        try {
            const {filter} = req.body;
            const result = await this.service.get(filter);
            return res.status(200).json({result:result});
        } catch (error) {
            return res.status(500).json({error:error});
        }
    }
}

export default BaseController;
