import jwt from "jsonwebtoken";
import MY_SECRET from "../config/jwt_private_key";
import controller from "../controllers/system/authorizationController";
const auth = (path, activity) => {
  return async (req, res, next) => {
    try {
      const token = req.header("token");
      // uncomment the line commented line below later on when using the env variables.
      const decoded = jwt.verify(token, MY_SECRET);
      // const decoded = jwt.verify(token, process.env.MY_SECRET);
      console.log("Decoded token is : ",decoded);
      req.role = decoded.role;
      req.id = decoded.id;
      if (path !== "") {
        req.authPath = path;
        await controller.checkAuth(req);
        if (req.authorized) {
          if (activity) {
            req.log = {
              actor: decoded.fullName,
              activity: activity,
              id: decoded.id,
            };
          }
          next();
        } else {
          return res
            .status(401)
            .json({ err: true, message: "Unauthorized Access!." });
        }
      } else {
        if (decoded.id) {
          next();
        } else {
          return res
            .status(401)
            .json({ err: true, message: "Unauthorized Access!." });
        }
      }
    } catch (error) {
      res.status(401).send({ error: "Unauthorized Access!" });
    }
  };
};

export default auth;
