import { resolve } from "path";
import dotenv from "dotenv";
import mongoose, { Schema } from "mongoose";
import bcrypt from "bcryptjs";

dotenv.config({ path: resolve(__dirname, "../.env") });
const jwt = require("jsonwebtoken");

class CustomerModel {
  constructor(){
    console.log("CustomerModel has been initialized...");
    this.model = null;
  }
  initSchema() {
    var Customerschema = new Schema(
      {
        companyName: {
          type: String,
          required: true,
        },
        projects: [
          {
            name: String,
            billOfQuantity: Number,
            placOfConstruction: Number,
            letterNumber: String,
            balance: Number,
            orderId: [
              {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Order",
              },
            ],
            constructionProgress: String,
          },
        ],
        class: {
          type: String,
        },
        address: [
          {
            country: {
              type: String,
              required: true,
              default: "Ethiopia",
            },
            region: {
              type: String,
              //required:true //region may not be required for out of Ethiopia customers
            },
            city: {
              type: String,
              required: true,
            },
            specificAdress: {
              type: String,
              required: true,
            },
            contactPerson: String,
            contactPhoneNumber: String,
          },
        ],
        // priority: [
        //   {
        //     priorityLevel: {
        //       type: String,
        //       default: "Regular",
        //     },
        //     status: {
        //       type: String,
        //       enum: ["Change Requested", "Active", "Inactive", "Rejected"],
        //       default: "Active",
        //     },
        //     log: [
        //       {
        //         actor: String,
        //         activity: String,
        //         activityDate: {
        //           type: Date,
        //           default: Date.now(),
        //         },
        //       },
        //     ],
        //   },
        // ],
        // paymentMethod: [
        //   {
        //     method: {
        //       type: String,
        //       enum: ["Credit", "Cash", "Loan", "Donation"],
        //       default: "Cash",
        //     },
        //     status: {
        //       type: String,
        //       enum: ["Change Requested", "Active", "Inactive", "Rejected"],
        //       default: "Active",
        //     },
        //     log: [
        //       {
        //         actor: String,
        //         activity: String,
        //         activityDate: {
        //           type: Date,
        //           default: Date.now(),
        //         },
        //       },
        //     ],
        //   },
        // ],
        licenseNumber: {
          type: String,
          unique: true,
        },
        tinNumber: {
          type: String,
          unique: true,
        },
        username: {
          type: String,
          //unique: true,
        },
        password: {
          type: String,
        },
        email: {
          type: String,
          match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        },
        status: {
          type: String,
          default: "Verified",
        },
        profileImg: String,
        reset_password_token: String,
        reset_password_expires: Date,
        // for password policy
        password_expired_date: Date,
        documents: [{ name: String }],
        log: [
          {
            actor: String,
            activity: String,
            activityDate: {
              type: Date,
              default: Date.now(),
            },
          },
        ],
      },
      { timestamps: true }
    );
    // const Account = mongoose.model("Customers", Customerschema);
    this.model = mongoose.model("Customers", Customerschema);
  }

  getModel() {
    this.initSchema();
    // return mongoose.model("Customers");
    return this.model;
  }
}

const customerModel = new CustomerModel().getModel();

// export default CustomerModel;
export default customerModel;
