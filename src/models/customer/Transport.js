import mongoose, { Schema } from "mongoose";  
import { date } from "joi";

class TransportModel {
    init() {
        let TransportSchema = new Schema({ 
            Name:String,
            AgreementDuration:{
                startDate:Date,
                endDate:Date
            },
            ContractNumber:String,
            status:{
                type:String,
                default:'Pending'
            },

        }, {timestamps: true});         
        const Transport = mongoose.model("Transport", DriverSchema);
    }

    getModel() {
        this.init();
        return mongoose.model("Transport");
    }
}

export default TransportModel 