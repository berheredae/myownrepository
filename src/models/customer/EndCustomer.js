import {resolve} from 'path';
import dotenv from "dotenv";
import mongoose, { Schema } from "mongoose";
import bcrypt from "bcryptjs";

dotenv.config({ path:  resolve(__dirname, '../.env')});
const jwt = require('jsonwebtoken'); 

class EndCustomerModel{
    initSchema() {
        var EndCustomerschema = new Schema({ 
            Name:{
                type:String,
                required:true
            },
            Projects:[
                {
                    name:String,
                    billOfQuantity:Number,
                    placOfConstruction:Number,
                    letterNumber:String,
                    balance:Number,
                    orderId:[
                        {
                            type:mongoose.Schema.Types.ObjectId,
                            ref:'Order'
                        }
                    ],
                    constructionProgress:String,
                }
            ],
            Class:{
                type:String
            },
            Address:[
            {
            Name:
            {
                type: String,
                required:true
            },
            county:
            {
                type: String,
                required:true,
                default:'Ethiopia'

            },
            region:
            {
                type: String,
                required:true
            },
            city:
            {
                type: String,
                required:true
            },
            specificAdress:
            {
                type: String,
                required:true
            },
            contactPerson:{
                Name:String,
                phoneNumber:String
            }

            }
        ],
        priorityNumber:Number,
        paymentMethod:[{
                type:String,
                enum:['Credit','Cash','Loan','Donation'],
                status:{
                    type:String,
                    default:'inactive',
                },
             settedDate:date
            },
        ],
            license:{
                licenseNumber:Number,
                licenseImage:String,
            },
            // email: {
            //     type: String,
            //     unique: true,
            //     match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            // },           
            // username: {
            //     type: String,
            //     unique: true
            // },
            // password: {
            //     type: String
            // }, 
            documents:[
               { name:String,}
            ],
            status: {
                type: String,
                default: "Inactive",
            }  
        }, {timestamps: true});
        
      
        
        const EndCustomer = mongoose.model("EndCustomers", Customerschema);
    }

    getModel() {
        this.initSchema();
        return mongoose.model("Customers");
    }

}

export default CustomerModel