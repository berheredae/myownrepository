import mongoose, { Schema } from "mongoose";  
import { date } from "joi";

class EmployeeModel {
    init() {
        let EmployeeSchema = new Schema({ 
            fullName:String,
            mobileNumber:String,
            employeeId:String,
            email: {
                type: String,
                unique: true,
                match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            },
            gender:{
                type:String,
                enum:['Male','Female']
            },
            department:String,
            division:String,
            office:String,
            role:{
                type:String
            },
            username: {
                type: String,
                unique: true
            },
            password: {
                type: String
            }, 
            profileImg:String,
            reset_password_token: String,
            //token:String,
            reset_password_expires: Date,

            // for password policy
            password_expired_date:Date,

            // permitted MAC 
            // allowes_mac1:String,
            // allowes_mac2:String,

            // temp_mac:{
            //     number:String,
            //     request_date:Date,
            //     "status": {
            //         type: String,
            //         default: "Active",
            //     }, 
            // },
            status: {
                type: String,
                default: "Verified",
            },
            log: [{
                actor:String,
                activity:String,
                activityDate: {
                    type:Date,
                    default: Date.now()
                }
            }]

        }, {timestamps: true});         
        const Employee = mongoose.model("Employee", EmployeeSchema);
    }

    getModel() {
        this.init();
        return mongoose.model("Employee");
    }
}

export default EmployeeModel 