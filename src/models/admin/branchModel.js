import mongoose, { Schema } from "mongoose";

class BranchModel {
  init() {
    let BranchSchema = new Schema({
      name: String,
      country: {
        type: String,
        required: true,
        default: "Ethiopia",
      },
      region: {
        type: String,
      },
      city: {
        type: String,
        required: true,
      },
      specificAddress: {
        type: String,
        required: true,
      },
      managerName: String,
      managerPhoneNumber: String,
      status: {
        type: String,
        default: "Unverified",
      },
      storeCapacity: Number,
      stores: [
       {
         storeId:{
           type:mongoose.Schema.Types.ObjectId,
           ref:"Store"
         }
       }
      ],
      log: [
        {
          actor: String,
          activity: String,
          activityDate: {
            type: Date,
            default: Date.now(),
          },
        },
      ],
    });
    const Branch = mongoose.model("Branches", BranchSchema);
  }

  getModel() {
    this.init();
    return mongoose.model("Branches");
  }
}

const branchModel = new BranchModel().getModel();
export default branchModel;

// export default BranchModel;
