import mongoose, { Schema } from "mongoose";

class ProductModel {
  constructor(){
    this.model = null;
  }
  init() {
    // status's are being set to default to a Verifed for testing purposes the default should be Unverifed.
    let productSchema = new Schema({
      productType: {
        type: String,
        unique: true,
      },
      description: String,
      productImg: String,
      status: {
        type: String,
        default: "Verified",
        enum: ["Unverified", "Suspended", "Verified"],
      },
      price: [
        {
          storeId: {
            type:mongoose.Schema.Types.ObjectId,
            ref:"Store"
          },
          packageType: String,
          unitPrice: Number,
          status: {
            type: String,
            default: "Verified",
            enum: ["Unverified", "Suspended", "Verified"],
          },
          log: [
            {
              actor: String,
              activity: String,
              activityDate: {
                type: Date,
                default: Date.now(),
              },
            },
          ],
        },
      ],
      ExportPrice: [
        {
          country: String,
          packageType: String,
          unitprice: Number,
          status: String,
          log: [
            {
              actor: String,
              activity: String,
              activityDate: {
                type: Date,
                default: Date.now(),
              },
            },
          ],
        },
      ],
      log: [
        {
          actor: String,
          activity: String,
          activityDate: {
            type: Date,
            default: Date.now(),
          },
        },
      ],
    });
    // const Product = mongoose.model("Products", productSchema);
    this.model =  mongoose.model("Products", productSchema);
  }

  getModel() {
    this.init();
    // return mongoose.model("Products");
    return this.model;
  }
}

const productModel = new ProductModel().getModel();


// export default ProductModel;
export default productModel;
