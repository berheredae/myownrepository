import CustomerModel from '../../models/customer/Customer';
import orderModel from '../../models/sales/commericialOrdersModel';
import branchModel from '../../models/admin/branchModel';
import storeModel from '../../models/inventory/storeModel';


class ReportService{
    constructor(){
        console.log("Reporting service has been initialized");
    }

    /***
     * returns all the customers which satisfy the specified filter/criteria
     */
    async getTotalCustomers(filter){
        const collFilter = {};
        if(filter.customerClass) collFilter['class'] = filter.customerClass;
        if(filter.status) collFilter['status'] = filter.status;
        const customers = await CustomerModel.find({...collFilter});
        return {customers:customers,count:customers.length};
    }

    async getOrders(filter){
        let collFilter = {};
        if(filter.status) collFilter['orders.status'] = filter.status;
        if(filter.orderType) collFilter['orders.orderType'] = filter.orderType;
        if(filter.productType) collFilter['orders.productType'] = filter.productType;
        const orders = await orderModel.find({...collFilter});
        
        return {orders,count:orders.length};
    }

    async getBranchs(filter){
        // will add filters when decided which filters are supported
        let collFilter = {};
        const branchs = await branchModel.find({...collFilter});
        return {branchs,count:branchs.length};
    }

    async getStores(branch_id,filter){
        const collFilter = {};
        const branch = await branchModel.findOne({_id:branch_id});
        if(!branch) return null;
        const branchStores = branch.stores;
        const stores =[];
        for(let s in branchStores){
            const store = await storeModel.findOne({_id:branchStores[s].storeId});
            stores.push(store);
        }
        return {stores,count:stores.length};
    }


}

export default ReportService;



