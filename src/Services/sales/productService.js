import CrudService from "../../lib/crud";
const jwt = require("jsonwebtoken");
import { imageValidator } from "../../validators/Joi/fileValidator";
import multer from "multer";
import storeModel from "../../models/inventory/storeModel";
class ProductService extends CrudService {
  constructor(model) {
    super(model);
  }
  async add(req) {
    try {
      let body = req.body;
      let { productType } = body;
      let productExist = await this.getByField({ productType: productType });
      if (!productExist.error) {
        return {
          error: true,
          message: "Product Type already Exists",
          statusCode: 200,
          record: null,
        };
      }
      body.log = req.log;
      let record = await this.create(body);
      return record;
    } catch (errormessage) {
      return {
        error: true,
        message: errormessage,
        statusCode: 400,
        record: null,
      };
    }
  }
  async modify(req) {
    try {
      let body = req.body;
      let id = req.params.id;
      let { productType } = body;
      let productExist = await this.getById(id);
      if (!productExist.error) {
        if (productType == productExist.record.productType) {
          let record = await this.update(id, req.body);
          await this.updateByAddSet(id, {
            log: {
              actor: req.log.actor,
              activity: req.log.activity,
              id: req.log.id,
            },
          });
          return record;
        } else {
          let productTypeExist = await this.getByField({
            productType: productType,
          });
          if (!productTypeExist.error) {
            return {
              error: true,
              message: "Product Type already Exists",
              statusCode: 200,
              record: null,
            };
          }
          let record = await this.update(id, req.body);
          await this.updateByAddSet(id, {
            log: {
              actor: req.log.actor,
              activity: req.log.activity,
              id: req.log.id,
            },
          });
          return record;
        }
      } else {
        return {
          error: true,
          message: "Please select the correct Item to modify",
          statusCode: 200,
          record: null,
        };
      }
    } catch (errormessage) {
      return {
        error: true,
        message: errormessage,
        statusCode: 400,
        record: null,
      };
    }
  }

  async setPrice(data){
    console.log("Data received :> ",data);
      const {id, store,packageType,unitPrice,log} = data;
      const product = await this.model.findById(id);
      if(!product) return null;
      // return price.storeId.toString() ===  store &&
      // price.packageType === packageType && 
      // parseFloat(price.unitPrice) === parseFloat(unitPrice)
      const recordExists = product.price.filter((price)=> {
        console.log("current price :> ",price);
        return price.storeId.toString() ===  store &&
        price.packageType === packageType 
      })
      console.log("Record exists >>> ",recordExists);
      if(recordExists.length > 0 ) return null;
      product.price.push({storeId:store,packageType:packageType,
                    unitPrice:parseFloat(unitPrice),log:log});
      product.save();
      // const u = this.model.updateOne({_id:id,'price.storeId':store,
      //             'price.packageType':packageType},{$push:{'price.$.log':log}});
      // console.log("U > ",u);
      return product;
  }

  // async setPrice(req) {
  //   try {
  //     let body = req.body;
  //     let id = req.params.id;
  //     let priceExist = await this.getByField({
  //       _id: id,
  //       "price.status": "Unverified",
  //       "price.packageType": body.packageType,
  //     });
  //     if (!priceExist.error) {
  //       return {
  //         error: true,
  //         message:
  //           "Please Verify the existing Price for this product and package type first",
  //         statusCode: 200,
  //         record: null,
  //       };
  //     } else {
  //       body.log = req.log;
  //       let record = this.updateByAddSet(id, { price: req.body });
  //       return record;
  //     }
  //   } catch (errormessage) {
  //     return {
  //       error: true,
  //       message: errormessage,
  //       statusCode: 400,
  //       record: null,
  //     };
  //   }
  // }
  // async modifyPrice(req) {
  //   try {
  //     let id = req.params.id;
  //     console.log("Looking for the product record..",id);
  //     let priceExist = await this.getByField({
  //       "price._id": id,
  //       "price.status": "Unverified",
  //     });
  //     console.log("Price exists :>",priceExist);
  //     console.log("and the log is :> ",req.log);
  //     if (priceExist.error) {
  //       return {
  //         error: true,
  //         message: "The price should be unverified to modify",
  //         statusCode: 200,
  //         record: null,
  //       };
  //     } else {
  //       let record = this.updateBySet(
  //         { "price._id": id },
  //         { "price.$": req.body }
  //       );
  //       await this.addByPush(
  //         { "price._id": id },
  //         {
  //           "price.$.log": {
  //             actor: req.log.actor,
  //             activity: req.log.activity,
  //             id: req.log.id,
  //           },
  //         }
  //       );
  //       return record;
  //     }
  //   } catch (errormessage) {
  //     console.log("Error (Modify Product price ",errormessage);
  //     return {
  //       error: true,
  //       message: errormessage,
  //       statusCode: 400,
  //       record: null,
  //     };
  //   }
  // }

  async modifyPrice(req) {
    try {
      let id = req.params.id;
      console.log("Looking for the product record..",id);
      let priceExist = await this.getByField({
        "price._id": id,
        "price.status": "Unverified",
      });
      console.log("Price exists :>",priceExist);
      console.log("and the log is :> ",req.log);
      if (priceExist.error) {
        return {
          error: true,
          message: "The price should be unverified to modify",
          statusCode: 200,
          record: null,
        };
      } else {
        let record = await this.updateBySet(
          { "price._id": id },
          // { "price.$": req.body }
          { "price.$.storeId": req.body.store,"price.$.packageType":req.body.packageType,"price.$.unitPrice":req.body.unitPrice }
        );
        console.log("Rec body::> ",record);
        // const rec = await this.model.findOne({"price._id":id});
        const rec = await this.model.updateOne({"price._id":id},{$push:{"price.$.log":{actor:req.log.actor,activity:req.log.activity,id:req.log.id}}});
        console.log("the record is :>>> ",rec);
        // rec.price.log.addToSet({actor:req.log.actor,activity:req.log.activity,id:req.log.id});
        // await this.addByPush(
        //   { "price._id": id },
        //   {
        //     "price.$.log": {
        //       actor: req.log.actor,
        //       activity: req.log.activity,
        //       id: req.log.id,
        //     },
        //   }
        // );
        return record;
      }
    } catch (errormessage) {
      console.log("Error (Modify Product price ",errormessage);
      return {
        error: true,
        message: errormessage,
        statusCode: 400,
        record: null,
      };
    }
  }


  async verifyPrice(req) {
    try {
      let id = req.params.id;
      let priceExist = await this.getByField({
        "price._id": id,
      });
      if (priceExist.error) {
        return {
          error: true,
          message: "Please select the correct the price to be modified",
          statusCode: 200,
          record: null,
        };
      } else {
        if (req.body.status == "Verified") {
          this.updateBySet(
            {
              "price.packageType": priceExist.record[0].price[0].packageType,
              "price.status": "Verified",
            },
            { "price.$.status": "Unverified" }
          );
        }
        let record = await this.updateBySet(
          { "price._id": id },
          { "price.$.status": req.body.status }
        );
        await this.addByPush(
          { "price._id": id },
          {
            "price.$.log": {
              actor: req.log.actor,
              activity: req.log.activity,
              id: req.log.id,
            },
          }
        );
        return record;
      }
    } catch (errormessage) {
      return {
        error: true,
        message: errormessage,
        statusCode: 400,
        record: null,
      };
    }
  }
  async imageUpload(req, res, next) {
    try {
      const storage = multer.diskStorage({
        destination: (req, file, cb) => {
          cb(null, "uploads/img/products");
        },
        filename: (req, file, cb) => {
          cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname);
        },
      });

      let upload = multer({
        storage: storage,
        fileFilter: imageValidator,
      }).single("productImg");

      upload(req, res, function (error) {
        if (req.fileValidationError) {
          return res
            .status(200)
            .send({ error: true, message: " Incorrect File Format" });
        } else if (!req.file) {
          return res.status(200).send({
            error: true,
            message: " Please select an image to upload",
          });
        } else if (error) {
          return res
            .status(200)
            .send({ error: true, message: " Internal Error" });
        }
        req.body.productImg = req.file.filename;
        next();
      });
    } catch (error) {
      return res.status(400).send({ error: true, message: " Internal Error" });
    }
  }
  async getSingle(req) {
    const result = await this.getByField(
      { _id: req.params.id },
      {
        log: 0,
        "price.log": 0,
        "ExportPrice.log": 0,
      }
    );
    if (!result.error) {
      let tempArr = [];
      await result.record[0].price.forEach((r) => {
        if (r.status == "Verified") {
          tempArr.push(r);
        }
      });
      let finalresult = {
        _id: result.record[0]._id,
        productType: result.record[0].productType,
        price: tempArr,
      };
      return {
        error: false,
        statusCode: 201,
        message: "Successfully retrieved",
        record: finalresult,
      };
    } else {
      return result;
    }
  }

  //new
  async getAllProductsWithStoreInfo() {
    console.log("get all products with store info model")
    console.log(this.model)
    const prods = await this.model.find({});
    const products = [];
    console.log("All Products are :>> ",prods);
    console.log("Prod ::>> ",prods[0].price[0].storeId);
    let prices = [];
    for (const p of prods) {
      prices = [];
      for(let price of p.price){
        console.log("Prod ::>> ", (price));
        const store = await storeModel.findOne({_id:price.storeId});
        console.log("Store :>> ",store);
        // price.storeName = store.storeName;
        prices.push({status:price.status,storeId:price.storeId,packageType:price.packageType,unitPrice:price.unitPrice,storeName:store.storeName});
      }
      products.push({_id:p._id,status:p.status,productType:p.productType,description:p.description,price:prices});
    }
    console.log("Products ::>> ",products);
    return {
      error: false,
      message: "Successfully retrieved record(s)",
      statusCode: 202,
      record:products,
    };
  }
}

export default ProductService;
