import CrudService from "../../lib/crud";
import branchModel from "../../models/admin/branchModel";
import storeModel from "../../models/inventory/storeModel";
class BranchService extends CrudService {
  constructor(model) {
    super(model);
  }

  async addStores(branch_id,store_ids){
      const branch = await branchModel.findById(branch_id);
      const stores = [...branch.stores];
      console.log("stores are :> ",stores);
      for (const store of store_ids) {
        const checkStore = await storeModel.findById(store);
        if(!checkStore) return null;
      }
      store_ids.forEach((store_id)=>{
        // so we don't store a duplicate.
        const exists = stores.find((store)=> store.storeId.toString() === store_id);
          if(!exists)
            stores.push({storeId:store_id});
      });
      branch.stores = stores;
      branch.save();
      return branch;
  }

  async deleteStore(branch_id,store_id){
    const branch = await branchModel.findById(branch_id);
    const filterStores = branch.stores.filter((store)=> store.storeId.toString() !== store_id);
    branch.stores = filterStores;
    branch.save();
    return branch;
  }


}

export default BranchService;
