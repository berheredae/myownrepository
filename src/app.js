import express from "express";
import cors from "cors";
import morgan from "morgan";
import fs from "fs";
import dotenv from "dotenv";
dotenv.config();
import Connection from "./config/database";
import messages from "./lib/messages.json";
import projectDependencies from './config/projectDependencies';
// import messages from './lib/messages.json';
import load_routes from "./routes";


const app = express();
const accessLogStream = fs.createWriteStream(__dirname + "/access.log", {
  flags: "a",
});

// Middlewares
app.use(morgan("combined", { stream: accessLogStream }));
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static(__dirname + "/public"));



async function start(){
  const dbcon = new Connection();
  // const con = dbcon.connect();
  // let con;
  // (async()=>{
    const con = await dbcon.connect();
    // console.log("Connection : ",con);
  // })();
  if (con) {
    console.log(messages.success.dbcon);
  } else {
    console.log(messages.error.dbcon, con, "  .. here");
  }
  load_routes(app);
  const PORT = process.env.PORT || 4000;  
  app.listen(PORT, () => {
    console.log(messages.success.servercon, PORT);
  });
  return app;
} 



export default start;
// export default app;
