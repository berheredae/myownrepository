// di injection.

import ReportService from "../Services/reporting/reportService"


const projectDependiencies = ()=>{
    console.log("project dependencies loading...");
    return {
        reportService: new ReportService(),
    }
}

export default projectDependiencies();