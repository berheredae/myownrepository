import {resolve} from 'path';
import dotenv from "dotenv";
dotenv.config({ path:  resolve(__dirname, '../.env')});

import mongoose from 'mongoose'

class Connection {
    async connect() { 
        const DB_NAME = process.env.DB_NAME
        // const DB_URL = process.env.DB_URL || `mongodb://localhost:27017/${DB_NAME}`
        const DB_URL = process.env.DB_URL || `mongodb://localhost:27017/salesmanagement`
 
        const options = {
            useCreateIndex: true,
            useFindAndModify: false,
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true
        }
        try {
          let con = await mongoose.connect(DB_URL , options) 
        //   console.log("Connection established :> ",con);
          return con
        } catch(err) {
            console.log("Error connecting to db :>",err);
          return err 
        }
    }

    async disconnect() {
        try {

            mongoose.connection.close()
            return true
        } catch (err) {
            return err
        }
    }
}

export default Connection